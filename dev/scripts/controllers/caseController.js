(function() {
  'use strict'
  angular.module('app').directive('case', caseController)

  function caseController(cases, $routeParams, auth, $timeout, aws) {
    return {

      templateUrl: 'pages/case.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this
        var ta = document.getElementById('obituary-textarea');
        autosize($(ta));

        angular.extend(vm, {
          //Variables and Functions
          tab: 'deceased',
          case: {
            details:{}
          },
          caseId: null,
          update: update,
          next: next,
          back: back,
          done: done,

          addChild: addChild,
          addSibling: addSibling,
          addOrganization: addOrganization,
          addMusic: addMusic,
          addPallBearers: addPallBearers,
          addOtherMedal: addOtherMedal,
          addUSStation: addUSStation,
          addFiringParty: addFiringParty,

          removeChild: removeChild,
          removeSibling: removeSibling,
          removeOrganization: removeOrganization,
          removeMusic: removeMusic,
          removePallBearer: removePallBearer,
          removeOtherMedal: removeOtherMedal,
          removeUSStation: removeUSStation,
          removeFiringParty: removeFiringParty,

          //done: done,
          saveAndExit: saveAndExit,

          //Accordians
          deceasedStatus: {
            isFirstOpen: false,
            isSecondOpen: false,
            isThirdOpen: false
          },
          serviceStatus: {
            isFirstOpen: false,
            isSecondOpen: false,
            isThirdOpen: false,
            isFourthOpen: false
          },
          militaryStatus: {
            isFirstOpen: false,
            isSecondOpen: false,
            isThirdOpen: false,
            isFourthOpen: false
          }
        })

        init()

        function init() {
          auth.verifyToken().then(function(valid){
            if(valid){
              getCase()
              detectImagesUpload()
            }
          })
        }

        function getCase() {
          cases.get($routeParams.id).then(function(response) {
            vm.case = response.data
            console.log(vm.case)
            if(!vm.case.details){
              vm.case.details = {}
              vm.case.details.children = []
              vm.case.details.siblings = []
              vm.case.details.music = []
              vm.case.details.pallbearers = []
              vm.case.details.othermedals = []
              vm.case.details.organizations = []
              vm.case.details.usstations = []
              vm.case.details.firingparties = []
              vm.case.personalImages = []
            }
          })
        }

        function update() {
          delete vm.case._id
          delete vm.case.caseId
          delete vm.case.created
          delete vm.case.__v
          cases.update($routeParams.id, vm.case).then(function(response){
            //console.log(response)
          })
        }

        function next() {
          if(vm.tab == 'deceased') {
            vm.tab = 'obituary'
          }
          else if(vm.tab == 'obituary') {
            vm.tab = 'service'
          }
          else {
            vm.tab = 'military'
          }
        }

        function back() {
          if(vm.tab == 'obituary') {
            vm.tab = 'deceased'
          }
          else if(vm.tab == 'service') {
            vm.tab = 'obituary'
          }
          else {
            vm.tab = 'service'
          }
        }

        function addChild() {
          var childrenLength
          if(vm.case.details.children.length) {
            childrenLength = vm.case.details.children.length + 1
          }
          else {
            childrenLength = 1
          }
          var object = {
            title: 'child ' + childrenLength,
            value: ''
          }
          vm.case.details.children.push(object)
        }

        function addSibling() {
          var siblingLength
          if(vm.case.details.siblings.length) {
            siblingLength = vm.case.details.siblings.length + 1
          }
          else {
            siblingLength = 1
          }
          var object = {
            title: 'sibling ' + siblingLength,
            value: ''
          }
          vm.case.details.siblings.push(object)
        }

        function addOrganization() {
          var organizationLength
          if(vm.case.details.organizations.length) {
            organizationLength = vm.case.details.organizations.length + 1
          }
          else {
            organizationLength = 1
          }
          var object = {
            title: 'organization ' + organizationLength,
            value: ''
          }
          vm.case.details.organizations.push(object)
        }

        function addMusic() {
          var musicLength
          if(vm.case.details.music.length) {
            musicLength = vm.case.details.music.length + 1
          }
          else {
            musicLength = 1
          }
          var arr = [
            {
              title: 'song type',
              value: ''
            },
            {
              title: 'music selection',
              value: ''
            },
            {
              title: 'special song ' + musicLength,
              value: ''
            },
            {
              title: 'rendered by',
              value: ''
            }
          ]
          vm.case.details.music.push(arr)
        }

        function addPallBearers() {
          var bearerLength
          if(vm.case.details.pallbearers.length) {
            bearerLength = vm.case.details.pallbearers.length + 1
          }
          else {
            bearerLength = 1
          }
          var arr = [
            {
              title: 'pall bearer',
              value: ''
            },
            {
              title: 'society attending ' + bearerLength,
              value: ''
            }
          ]
          vm.case.details.pallbearers.push(arr)
        }

        function addOtherMedal() {
          var otherMedalLength
          if(vm.case.details.othermedals.length) {
            otherMedalLength = vm.case.details.othermedals.length + 1
          }
          else {
            otherMedalLength = 1
          }
          var object = {
            title: 'Other Medal ' + otherMedalLength,
            value: ''
          }
          vm.case.details.othermedals.push(object)
        }

        function addUSStation() {
          var stationLength
          if(vm.case.details.usstations.length) {
            stationLength = vm.case.details.usstations.length + 1
          }
          else {
            stationLength = 1
          }
          var object = {
            title: 'US Station ' + stationLength,
            value: ''
          }
          vm.case.details.usstations.push(object)
        }

        function addFiringParty() {
          var partyLength
          if(vm.case.details.firingparties.length) {
            partyLength = vm.case.details.firingparties.length + 1
          }
          else {
            partyLength = 1
          }
          var object = {
            title: 'Firing Party ' + partyLength,
            value: ''
          }
          vm.case.details.firingparties.push(object)
        }

        //Removal Functions

        function removeChild(object, array) {
          vm.case.details.children = _.remove(vm.case.details.children, function(interval) {
            return interval !== object
          })
        }

        function removeSibling(object, array) {
          vm.case.details.siblings = _.remove(vm.case.details.siblings, function(interval) {
            return interval !== object
          })
        }

        function removeOrganization(object, array) {
          vm.case.details.organizations = _.remove(vm.case.details.organizations, function(interval) {
            return interval !== object
          })
        }

        function removeMusic(object, array) {
          vm.case.details.music = _.remove(vm.case.details.music, function(interval) {
            return interval !== object
          })
        }

        function removePallBearer(object, array) {
          vm.case.details.pallbearers = _.remove(vm.case.details.pallbearers, function(interval) {
            return interval !== object
          })
        }

        function removeOtherMedal(object, array) {
          vm.case.details.othermedals = _.remove(vm.case.details.othermedals, function(interval) {
            return interval !== object
          })
        }

        function removeUSStation(object, array) {
          vm.case.details.usstations = _.remove(vm.case.details.usstations, function(interval) {
            return interval !== object
          })
        }

        function removeFiringParty(object, array) {
          vm.case.details.firingparties = _.remove(vm.case.details.firingparties, function(interval) {
            return interval !== object
          })
        }

        function saveAndExit() {
          update()
          $timeout(function(){
            window.location.href = '/cases'
          },500)
        }

        function done() {
          vm.case.inProgress = false
          update()
          $timeout(function(){
            window.location.href = '/cases'
          },500)
        }

        function detectImagesUpload() {
          $timeout(function() {
            document.getElementById("personal_images").onchange = function() {
              var files = document.getElementById("personal_images").files
							var currentRequest = 0

							function iterate() {
					      if (currentRequest < files.length) {
									var file = files[currentRequest]
		              if (file == null) {
		                alert("No file selected.")
		              } else {
		                var filename = Math.random().toString(36).substring(7) + file.name
										aws.getSignedUrl(file, filename).then(function(response){
		                  aws.uploadImage(file, response.data.signed_request).then(function(){
                        if(!vm.case.details.personalImages){
                          vm.case.details.personalImages = []
                        }
                        vm.case.details.personalImages.push(response.data.url)
                        currentRequest++
                        iterate()
		                  })
		                })
		              }
					      } else {
									console.log('done')
					      }
					    }
					    iterate()
            }
          })
        }

        //******** end *********//
      }
    }
  }

})();
