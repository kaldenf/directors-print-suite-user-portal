(function() {
  'use strict'
  angular.module('app').directive('cases', casesController)

  function casesController(cases, $filter, auth, $timeout, aws, $rootScope) {
    return {

      templateUrl: 'pages/cases.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          cases: null,
          createCase: createCase,
          deleteCase: deleteCase,
          search: {},
          importCSV: importCSV
        })

        init()

        function init(){
          getCases()
          detectFileUpload()
        }

        function getCases(){
          cases.getAll().then(function(response){
            response.data = $filter('orderBy')(response.data, 'caseId', true)
            vm.cases = response.data
            console.log(response.data)
          })
        }

        function createCase() {
          cases.create().then(function(response) {
            if (response.data) {
              window.location.href = '/cases/case/' + response.data._id
            }
          })
        }

        function deleteCase(id, index) {
          if(confirm('are you sure you want to delete this case?')){
            cases.delete(id).then(function() {
              vm.cases.splice(index, 1)
            })
          }
        }

        function importCSV() {
          $timeout(function(){
						document.getElementById('csvfile').click()
					},10)
        }

        function detectFileUpload() {
          $timeout(function() {
            document.getElementById("csvfile").onchange = function() {
              console.log('changed!')
              var files = document.getElementById("csvfile").files
              var file = files[0]
              if (file == null) {
                alert("No file selected.")
              } else {
                var filename = Math.random().toString(36).substring(7) + file.name
                aws.getCSVSignedUrl(file, filename).then(function(response){
                  console.log('get signed url')
                  aws.uploadImage(file, response.data.signed_request).then(function(){
										console.log(response.data.url)
                    getCSVData(response.data.url)
                  })
                })
              }
            }
          })
        }

        function getCSVData(url){
          cases.getCSVData(url).then(function(response){
            console.log(response)
          })
        }

        //******** end *********//
      }
    }
  }

})();
