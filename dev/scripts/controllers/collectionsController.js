(function() {
	'use strict'
	angular.module('app').directive('collections', collectionsController)

	function collectionsController(collections, products, orders, $q, auth) {
		return {

			templateUrl: 'pages/collections.html',
			controllerAs: 'vm',
			controller: function() {
				var vm = this

				angular.extend(vm, {
					collections: null,
					tab: null,
					setTab: setTab,
					order: {
						creatorId: localStorage.getItem('userId'),
						theme: null,
						caseId: null,
						products: []
					},
					productCount: 0,
					removeProductFromOrder: removeProductFromOrder,
					startOrder: startOrder,
					saveOrderAndExit: saveOrderAndExit,
					updateProductCount: updateProductCount
				})

				init()

				function init() {
					auth.verifyToken().then(function(valid){
            if(valid){
							enableMenuClick()
							getCollections()
								.then(function(collections) {
									getCollectionProducts(collections)
								})
            }
          })

				}

				function getCollections() {
					var deferred = $q.defer()
					collections.getAll().then(function(data) {
						deferred.resolve(data)
					})
					return deferred.promise
				}

				function getCollectionProducts(collections) {
					products.getCollectionProducts(collections).then(function(data) {
						vm.order.collections = data.data
						vm.tab = data.data[0].name
						loadSavedOrder()
						updateProductCount()
					})
				}

				function setTab(name) {
					vm.tab = name
				}

				function removeProductFromOrder(product) {
					product.selected = false
					updateProductCount()
				}

				function updateProductCount() {
					var count = 0
					for(var i in vm.order.collections){
						for(var j in vm.order.collections[i].products){
							if(vm.order.collections[i].products[j].selected){
								count++
							}
						}
					}
					vm.productCount = count
				}

				function startOrder() {
          localStorage.removeItem('order')
          localStorage.removeItem('selectedProducts')
					orders.create(vm.order).then(function(response) {
						window.location.href = '/order/' + response.data
					})
				}

				function enableMenuClick() {
					setTimeout(function() {
						$(document).on('click', '.dropdown .dropdown-menu', function(e) {
							e.stopPropagation()
						});
					}, 100)
				}

				function saveOrderAndExit() {
					localStorage.order = JSON.stringify(vm.order)
          window.location.href = '/dashboard'
				}

				function loadSavedOrder() {
					if (localStorage.order) {
						vm.order = JSON.parse(localStorage.order)
						console.log()
					}
				}

				//******** end *********//
			}
		}
	}

})();
