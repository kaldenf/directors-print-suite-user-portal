(function() {
	'use strict'
	angular.module('app').directive('dashboard', dashboardController)

	function dashboardController(orders, cases, auth, $q) {
		return {

			templateUrl: 'pages/dashboard.html',
			controllerAs: 'vm',
			controller: function() {
				var vm = this

				angular.extend(vm, {
					orders: null,
					createNewOrder: createNewOrder
				})

				init()

				function init() {
					auth.verifyToken().then(function(valid) {
						if (valid) {
							getOrders()
								.then(function(orders) {
									getCasesForOrders(orders)
								})
						}
					})

				}

				function getOrders() {
					var deferred = $q.defer()
					orders.getRecent(10).then(function(orders) {
						console.log(orders)
						deferred.resolve(orders)
					})
					return deferred.promise
				}

				function getCasesForOrders(orders) {
					cases.getCasesForOrders(orders.data).then(function(response) {
						vm.orders = response
						console.log(vm.orders)
					})
				}

				function createNewOrder() {
					var object = {
						creatorId: localStorage.getItem('userId'),
						theme: null,
						caseId: null,
						collections: null
					}
					orders.create(object).then(function(response) {
						window.location.href = '/order/' + response.data
					})
				}

				//******** end *********//
			}
		}
	}

})();
