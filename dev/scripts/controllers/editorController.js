(function() {
  'use strict'
  angular.module('app').directive('editor', editorController)

  function editorController(auth, $routeParams, orders, $rootScope, assetLibraries, $q, assets, $compile, $document, cases, $timeout) {
    return {
      templateUrl: 'pages/editor.html',
      scope: true,
      link: function(scope) {
        var vm = scope

        angular.extend(scope, {
          //Variables and Functions
          order : null,
          case: null,
					assetLibraries : [],
					selected : {},
					showFontColorPicker : showFontColorPicker,
					showBorderColorPicker: showBorderColorPicker,
					selectElement : selectElement,
					deselectElements: deselectElements,
					deleteElement: deleteElement,
					textActions : {
						rightAlign: rightAlignText,
						center: centerText,
						justify: justifyText,
						leftAlign: leftAlignText,
						leftAlignObject: leftAlignObject,
						centerVerticalAlignObject: centerVerticalAlignObject,
						rightAlignObject: rightAlignObject,
						topAlignObject: topAlignObject,
						centerHorizontalAlignObject: centerHorizontalAlignObject,
						bottomAlignObject: bottomAlignObject,
						updateFont: updateFont
					},
          imageActions: {
            ovalShape: ovalShape,
            squareShape: squareShape
          },
					updateSelected: updateSelected,
					addPicture: addPicture,
					fonts: []
        })

        init()

        function init(){
          auth.verifyToken().then(function(valid){
            if(valid){
              getOrder().then(function(){
                getCaseInfo()
              })
							getAssetLibraries().then(function(response){
								populateAssetsInLibrary(response.data)
								setEditorHeight()
								setDraggables()
								setSelectProducts()
                setResizerDrag()
							})
            }
          })

        }

        function getCaseInfo() {
          if(vm.order.caseId){
            cases.get(vm.order.caseId).then(function(response){
              vm.case = response.data
              console.log(vm.case)
            })
          }
        }

				function addPicture(file) {
					var image = $compile('<img src="https://s3-us-west-2.amazonaws.com/bassmollett-uploads/'+file+'" style="position:absolute; max-width: 100%;" data-type="image" class="draggable element"/>')(vm)
					$('.page').append(image)
				}

				function deselectElements(){
					//$('*').removeClass('selected')
				}

				function selectElement(element){
          vm.selected = {}
					var obj = element.target
					var selected = $(obj)
          var type = selected.data('type')

          //Global
          vm.selected.width = parseInt(selected.css('width'))
          vm.selected.height = parseInt(selected.css('height'))
          if(selected.css('border')){
            var border = selected.css('border').split(' ')
            vm.selected.border = {}
            vm.selected.border.active = true
            vm.selected.border.type = border[1]
            vm.selected.border.thickness = parseInt(border[0])
            vm.selected.border.color = border[2]
          }

          if(type == 'image'){
            //Show correct Action bar
            vm.action_type = 'image'

            $('*').removeClass('resizer-target')
            selected.addClass('resizer-target')
            positionResizer()
          } else if (type == 'text'){
            $('.resizer').css('display','none')
            //Show correct Action bar
            vm.action_type = 'text'

            //Get Attributes
  					vm.selected.text = selected.html()
  					vm.selected.textAlign = selected.css('text-align')
  					vm.selected.fontsize = parseInt(selected.css('font-size'))
          } else {
            $('.resizer').css('display','none')
          }

          vm.selected.element = obj
					$('.element').removeClass('selected')
					selected.addClass('selected')
				}

        function updateSelected() {
					var selected = $(vm.selected.element)
          var type = selected.data('type')

          //Global
          selected.css('width', vm.selected.width + 'px')
          selected.css('z-index', vm.selected.zindex)
          if(vm.selected.border.active){
            selected.css('border', vm.selected.border.type + ' ' + vm.selected.border.thickness + 'px ' + vm.selected.border.color )
          } else {
            selected.css('border','none')
          }

          //Type Specific
          if(type == 'image'){
            vm.selected.height = parseInt(selected.css('height'))
          } else if (type == 'text'){
  					selected.css('height', vm.selected.height + 'px')
  					selected.css('font-size', vm.selected.fontsize + 'px')
  					selected.css('color', vm.selected.fontcolor)
          }
				}

        function positionResizer(){
          var target = $('.resizer-target')
          var top = parseInt(target.css('height')) + parseInt(target.css('top'))
          var left = parseInt(target.css('width')) + parseInt(target.css('left'))
          $('.resizer').css({
            top: top,
            left: left,
            display: 'block'
          })
        }

				function deleteElement() {
					$(vm.selected.element).remove()
				}

        function getOrder() {
          var deferred = $q.defer()
          orders.get($routeParams.orderid).then(function(response){
            vm.order = response.data
            for(var i in response.data.collections){
              var products = response.data.collections[i].products
              for(var j in products){
                if(products[j]._id == $routeParams.productid){
                  vm.product = products[j]
                  var el = $compile(vm.product.layouts[0].pages[0].template)(vm)
                  $('#editor .product').append(el)
                  $timeout(function(){
                    sizeImages()
                  },200)

                }
              }
            }
            deferred.resolve(true)
          })
          return deferred.promise
        }

        function sizeImages(){
          $.each($('.product .image'), function(){
            var image = $(this)
            var src = image.css('background-image').replace(/url\((['"])?(.*?)\1\)/gi, '$2').split(',')[0];
             // I just broke it up on newlines for readability

             var image = new Image();
             image.src = src;

             var width = image.width,
                 height = image.height,
                 pageWidth = parseInt($('#editor .page').css('width'))
            if(width > pageWidth){
              var ratio = pageWidth / width
              width = pageWidth
              height = height * ratio
            }
             $(this).css({
               width: width + 'px',
               height: height + 'px'
             })
          })



        }

				function getAssetLibraries() {
					return $q.when(assetLibraries.getAll())
				}

				function populateAssetsInLibrary(libraries) {
					for(var i in libraries){
						assets.getLibraryAssets(libraries[i]._id).then(function(response){
							libraries[i].assets = response.data
						})
					}
					vm.assetLibraries = libraries
				}

				function showFontColorPicker(e) {
					e.stopPropagation()
					vm.showFontColor = !vm.showFontColor
				}

				function showBorderColorPicker(e) {
					e.stopPropagation()
					vm.showBorderColor = !vm.showBorderColor
				}

				function setEditorHeight(){
					var height = $(window).height()-60
					$('#editor').css('height', height)
					$(window).resize(function(){
						setEditorHeight()
					})
				}

				function setSelectProducts() {
					$('.product').on('mousedown', '.element', function($event){
						selectElement($event)
					})
				}



        function setResizerDrag() {
          $('.product').on('mousedown', '.resizer', function($event){
            var startWidth, startHeight, initialMouseX, initialMouseY;

            startWidth = parseInt($('.resizer-target').css('width'))
            startHeight = parseInt($('.resizer-target').css('height'))
						initialMouseX = $event.clientX;
						initialMouseY = $event.clientY;

            $document.bind('mousemove', mousemove);
						$document.bind('mouseup', mouseup);

            function mousemove($event) {
							var dx = $event.clientX - initialMouseX;
							var dy = $event.clientY - initialMouseY;

              positionResizer()
							$('.resizer-target').css({
								width: startWidth + dx + 'px',
                height: startHeight + dx + 'px'
							});

							return false;
						}

						function mouseup() {
							$document.unbind('mousemove', mousemove);
							$document.unbind('mouseup', mouseup);
						}
          })
        }

				function setDraggables() {
					$('.product').on('mousedown', '.draggable', function($event){

						var startX, startY, initialMouseX, initialMouseY;

						var target = $event.target

						$(target).css({
							'position': 'absolute',
							'cursor': 'pointer',
							'opacity': '0.5',
						});

						startX = $(target).prop('offsetLeft');
						startY = $(target).prop('offsetTop');
						initialMouseX = $event.clientX;
						initialMouseY = $event.clientY;

						$document.bind('mousemove', mousemove);
						$document.bind('mouseup', mouseup);

						// elm.dblclick(function(){
						// 	elm.addClass('edit-state')
						// 	$document.unbind('mousemove', mousemove);
						// 	$document.unbind('mouseup', mouseup);
						// })

						function mousemove($event) {
							var dx = $event.clientX - initialMouseX;
							var dy = $event.clientY - initialMouseY;
              positionResizer()
							$(target).css({
								top: startY + dy + 'px',
								left: startX + dx + 'px'
							});
							return false;
						}

						function mouseup() {
							$document.unbind('mousemove', mousemove);
							$document.unbind('mouseup', mouseup);
							$(target).css({
								opacity: 1
							})
						}

					})


				}


				//Action Bar functions
				function rightAlignText(){
					$(vm.selected.element).css('text-align','right')
				}

				function centerText(){
					$(vm.selected.element).css('text-align','center')
				}

				function justifyText(){
					$(vm.selected.element).css('text-align','justify')
				}

				function leftAlignText(){
					$(vm.selected.element).css('text-align','left')
				}

				function leftAlignObject() {
					$(vm.selected.element).css('right','auto')
					$(vm.selected.element).css('left','0px')
				}

				function centerVerticalAlignObject() {
					var containerHeight = $('.page').css('height')
					$(vm.selected.element).css('top', parseInt(containerHeight)/2 - parseInt($(vm.selected.element).css('height'))/2 + 'px')
				}

				function rightAlignObject() {
					$(vm.selected.element).css('left','auto')
					$(vm.selected.element).css('right','0px')
				}

				function topAlignObject() {
					$(vm.selected.element).css('bottom','auto')
					$(vm.selected.element).css('top','0px')
				}

				function centerHorizontalAlignObject() {
					var containerWidth = $('.page').css('width')
					$(vm.selected.element).css('left', parseInt(containerWidth)/2 - parseInt($(vm.selected.element).css('width'))/2 + 'px')
				}

				function bottomAlignObject() {
					$(vm.selected.element).css('top','auto')
					$(vm.selected.element).css('bottom','0px')
				}

				function updateFont(){
					$(vm.selected.element).css('font-family', vm.fonts[vm.selectedFont].name)
					if(vm.fonts[vm.selectedFont].weights){
						if(vm.selectedWeight){
							$(vm.selected.element).css('font-weight', vm.fonts[vm.selectedFont].weights[vm.selectedWeight].weight)
						} else {
							$(vm.selected.element).css('font-weight', vm.fonts[vm.selectedFont].weights[0].weight)
							vm.selectedWeight = 0
						}
					} else {
						$(vm.selected.element).css('font-weight', 'normal')
					}
				}

        function ovalShape(){
          $(vm.selected.element).css('border-radius','50%')
        }

        function squareShape(){
          $(vm.selected.element).css('border-radius','0px')
        }

				vm.fonts = [
					{
						'name':'AG University'
					},{
						'name':'Amazone'
					},{
						'name':'Century Gothic',
						'weights':[
							{
								'name':'Normal',
								'weight':500
							},{
								'name':'Bold',
								'weight':800
							},{
								'name':'Bold Italic',
								'weight':700
							},{
								'name':'Italic',
								'weight':600
							}]
					},{
						'name':'English'
					},{
						'name':'FG Jayne'
					},{
						'name':'Gaeilge'
					},{
						'name':'Goudy Catalog'
					},{
						'name':'Liberty'
					},{
						'name':'Mistral'
					},{
						'name':'Monterey'
					},{
						'name':'Murray Hill',
						'weights':[
							{
								'name':'Normal',
								'weight':'Normal'
							},{
								'name':'Bold',
								'weight':'Bold'
							}]
					},{
						'name':'Nuptial'
					},{
						'name':'Papyrus'
					},{
						'name':'Park Avenue'
					},{
						'name':'Piranesi'
					},{
						'name':'Pristina'
					},{
						'name':'Ribbon'
					},{
						'name':'Ruthie'
					},{
						'name':'Scriptina'
					},{
						'name':'Shelley Allegro'
					},{
						'name':'Shelley Andante'
					},{
						'name':'Vivaldii'
					},{
						'name':'Wedding Text'
					}
				]

        //******** end *********//
      }

    }
  }

})();
