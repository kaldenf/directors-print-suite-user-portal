(function() {
  'use strict'
  angular.module('app').directive('siteheader', headerController)

  function headerController() {
    return {

      templateUrl: 'partials/header.html',
      controllerAs: 'vms',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
        })


        //******** end *********//
      }
    }
  }

})();
