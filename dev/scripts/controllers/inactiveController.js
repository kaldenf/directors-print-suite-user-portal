(function() {
  'use strict'
  angular.module('app').directive('inactive', inactiveController)

  function inactiveController() {
    return {

      templateUrl: 'pages/inactive.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
        })


        //******** end *********//
      }
    }
  }

})();
