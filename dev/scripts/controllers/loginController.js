(function() {
  'use strict'
  angular.module('app').directive('login', loginController)

  function loginController(user, $q) { //add defined services here
    return {

      templateUrl: 'pages/login.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          form: {},
          login: login
        })



        function login() {
          vm.loggingin = true
          user.login(vm.form).then(function(data) { //run service function
            if (!data) {
              vm.loggingin = false
              console.log('incorrect Username or Password')
            } else {
              vm.loggingin = false
              window.location.href = '/dashboard'
            }
          })
        }

        //******** end *********//
      }
    }
  }

})();
