(function() {
  'use strict'
  angular.module('app').directive('mastercontroller', masterController)

  function masterController($rootScope, user) {
    return {
      controllerAs: 'mvm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          test: 'hello',
          logout: logout
        })

        getLocation()

        function getLocation(){
          if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
              //console.log(position)
            });
          } else {
            /* geolocation IS NOT available */
          }
        }

        function logout(){
          console.log('logout')
          user.logout()
        }

        //******** end *********//
      }
    }
  }

})();
