(function() {
  'use strict'
  angular.module('app').directive('onboarding', onboardingController)

  function onboardingController() {
    return {

      templateUrl: 'pages/onboarding.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
        })


        //******** end *********//
      }
    }
  }

})();
