(function() {
	'use strict'
	angular.module('app').directive('order', orderController)

	function orderController(orders, themes, themeCategories, cases, $routeParams, $q, collections, products, auth) {
		return {

			templateUrl: 'pages/order.html',
			controllerAs: 'vm',
			controller: function() {
				var vm = this

				angular.extend(vm, {
					order: {},
					tab: 'case',
					categories: null,
					showSlider: false,
					createCase: createCase,
					next: next,
          back: back,
					idSelectedCase: null,
					setSelectedCase: setSelectedCase,
					selectTheme: selectTheme,
					cases: null,
					selectCase: selectCase,
          saveOrderAndExit: saveOrderAndExit,
					selectProduct: selectProduct,
					selectSize: selectSize,
					done: done
				})

				init()

				function init() {
					auth.verifyToken().then(function(valid){
            if(valid){
							getOrder()
							.then(function(){
								getCollections()
									.then(function(collections) {
										getCollectionProducts(collections)
									})
							})
							getThemes()
							getCases()
							showSlider()

            }
          })
				}

				function selectProduct(product, collection){
					vm.selectedProduct = product
					vm.selectedProductCollection = collection
					vm.selectedSize = product.layouts[0].name
				}

				function selectSize(layout) {
					vm.selectedSize = layout.name
				}

				function showSlider() {
					setTimeout(function() {
						vm.showSlider = true
					}, 1000)
				}

				function getOrder() {
					var deferred = $q.defer()
					orders.get($routeParams.id).then(function(order) {
						vm.order = order.data
						deferred.resolve(true)
					})
					return deferred.promise
				}

				function getCollections() {
					var deferred = $q.defer()
					collections.getAll().then(function(data) {
						deferred.resolve(data)
					})
					return deferred.promise
				}

				function getCollectionProducts(collections) {
					products.getCollectionProducts(collections).then(function(data) {
						if(!vm.order.collections){
							vm.order.collections = data.data
						}
						vm.list = vm.order.collections[0].name
						selectProduct(vm.order.collections[0].products[0])
						vm.collectiontab = data.data[0].name
					})
				}

				function getThemes() {
					themeCategories.getAll().then(function(categories) {
						themes.getThemesForCategories(categories).then(function(response) {
							vm.categories = response.data
						})
					})
				}

				function getCases() {
					cases.getAll().then(function(cases) {
						vm.cases = cases.data
					})
				}

				function selectTheme(theme, category) {
					var object = {
						theme: theme.name,
						category: category.name,
						image: theme.image,
						id: theme._id
					}
					vm.order.theme = object
					updateOrder()
				}

				function next() {
          if(vm.tab == 'case') {
            vm.tab = 'products'
          }
          else if(vm.tab == 'products') {
            vm.tab = 'theme'
          }
          else {
            vm.tab = 'customize'
          }
        }

        function back() {
          if(vm.tab == 'products') {
            vm.tab = 'case'
          }
          else if(vm.tab == 'theme') {
            vm.tab = 'products'
          }
          else {
            vm.tab = 'theme'
          }
        }

				function setSelectedCase(idSelectedCase) {
					vm.idSelectedCase = idSelectedCase
				}

				function selectCase(selectedCase) {
					vm.order.caseId = selectedCase._id
					console.log(vm.order)
					updateOrder()
				}

				function updateCaseInfo() {

					//If order has case id, then update all product templates with new information
				}

        function updateOrder() {
          orders.update(vm.order).then(function(response) {
            console.log(vm.order)
					})
        }

				function done(){
					orders.update(vm.order).then(function(response) {
						window.location.href = '/dashboard'
					})
				}

				function saveOrderAndExit() {
					orders.update(vm.order).then(function(response) {
						window.location.href = '/dashboard'
					})
				}

				function createCase() {
					orders.update(vm.order).then(function(){
						cases.create().then(function(response) {
	            if (response.data) {
	              window.location.href = '/cases/case/' + response.data._id
	            }
	          })
					})
				}



				//******** end *********//
			}
		}
	}

})();
