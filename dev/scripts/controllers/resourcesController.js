(function() {
  'use strict'
  angular.module('app').directive('resources', resourcesController)

  function resourcesController() {
    return {

      templateUrl: 'pages/resources.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
        })


        //******** end *********//
      }
    }
  }

})();
