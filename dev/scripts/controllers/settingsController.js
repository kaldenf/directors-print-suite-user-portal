(function() {
  'use strict'
  angular.module('app').directive('settings', settingsController)

  function settingsController(user, funeralHomes, $timeout, auth) {
    return {

      templateUrl: 'pages/settings.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          tab: 'staff',
          form: {},
          addStaff: addStaff,
          staffMembers: null,
          removeStaff: removeStaff,
          home: {},
          resendActivationEmail: resendActivationEmail,
          updateFuneralHome: updateFuneralHome,
          user: {},
          updateUser: updateUser,
          saving: null,
          error: null
        })

        init()

        function init() {
          auth.verifyToken().then(function(valid){
            if(valid){
              getStaffMembers()
              getHomeInfo()
              detectLogoUpload()
              getUser()
            }
          })

        }

        function getStaffMembers() {
          console.log('getting staff members')
          user.getStaffMembers().then(function(members) {
            console.log(members.data)
            vm.staffMembers = members.data
          })
        }

        function addStaff() {
          vm.saving = true
          vm.error = null
          console.log(validateEmail(vm.form.email))
          if(!vm.form.name){
            vm.saving = false
            vm.error = 'Please enter a name'
          } else if(!validateEmail(vm.form.email)){
            vm.saving = false
            vm.error = 'Please enter a valid email'
          } else {
            user.addStaff(vm.form).then(function(data) {
              $timeout(function(){
                vm.saving = false
                vm.form = {}
                getStaffMembers()
              }, 1000)
            })
          }
        }

        function removeStaff(id) {
          if (confirm('Are you sure you want to delete this user?')) {
            user.removeStaff(id).then(function(data) {
              getStaffMembers()
            })
          }
        }

        function resendActivationEmail(activationId, funeralHomeName, email){
          user.resendActivationEmail(activationId, funeralHomeName, email).then(function(){
            console.log('email sent')
          })
        }

        function getHomeInfo() {
          funeralHomes.get().then(function(response){
            console.log(response)
            user.getStaffMembers().then(function(members){
              vm.home = response.data
              for(var i in members.data){
                if(members.data[i].manager){
                  vm.home.manager = members.data[i].name
                }
              }
            })
          })
        }

        function updateFuneralHome() {
          vm.saving = true
          vm.homeerror = null
          if(!vm.home.name){
            vm.saving = false
            vm.homeerror = 'Please enter a valid funeral home name'
          } else {
            funeralHomes.update(vm.home).then(function(){
              vm.saving = false
              console.log('complete')
            })
          }

        }

        function detectLogoUpload() {
          $timeout(function() {
            document.getElementById("logo_file").onchange = function() {
              var files = document.getElementById("logo_file").files
              var file = files[0]
              if (file == null) {
                alert("No file selected.")
              } else {
                var filename = Math.random().toString(36).substring(7) + file.name
                funeralHomes.getSignedUrl(file, filename).then(function(response){
                  funeralHomes.uploadImage(file, response.data.signed_request).then(function(){
                    vm.home.logo = response.data.url
                  })
                })
              }
            }
          })
        }

        function getUser() {
          user.get().then(function(user){
            vm.user = user.data
          })
        }

        function updateUser() {
          vm.saving = true
          vm.usererror = null
          if(!vm.user.name){
            vm.saving = false
            vm.usererror = 'Please enter a valid name.'
          } else if(!validateEmail(vm.user.email)) {
            vm.saving = false
            vm.usererror = 'Please enter a valid email.'
          } else {
            user.update(vm.user).then(function(){
              vm.saving = false
            })
          }
        }

        //******** end *********//
      }
    }
  }

})();
