(function() {
  'use strict'
  angular.module('app').directive('signup', signupController)

  function signupController(user) {
    return {

      templateUrl: 'pages/signup.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          form: {},
          signup: signup
        })

        user.getIpLocation().then(function(response){
          console.log(response)
        })

        function signup(){

          if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
              vm.form.details.position = position.coords
              finish()
            }, function(){
              user.getIpLocation().then(function(response){
                vm.form.details.position = response.data
                finish()
              })
            });
          } else {
            user.getIpLocation().then(function(response){
              vm.form.details.position = response.data
              finish()
            })
          }

          function finish(){
            vm.form.active = false
  					user.checkEmail(vm.form.email).then(function(response){
  						if(response.data.success){
  							user.signup(vm.form).then(function(data){
  		            console.log(data)
  		          })
  						} else {
  							vm.message = 'Email already in use'
  						}
  					})
          }


        }

        //******** end *********//
      }
    }
  }

})();
