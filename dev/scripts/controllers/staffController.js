(function() {
  'use strict'
  angular.module('app').directive('staff', staffController)

  function staffController(user, $routeParams) {
    return {

      templateUrl: 'pages/staff.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          form:{},
          activateUser: activateUser
        })

        function activateUser() {
          user.activate($routeParams.id, vm.form).then(function(message){
            console.log(message)
          })
        }

        //******** end *********//
      }
    }
  }

})();
