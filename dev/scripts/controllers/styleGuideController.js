(function() {
  'use strict'
  angular.module('app').directive('styleGuide', styleGuideController)

  function styleGuideController() {
    return {

      templateUrl: 'pages/style-guide.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          tab: 'forms',

          //Collapse
          isCollapsed: false,

          //Accordian
          oneAtATime: true,
          status: {
            isFirstOpen: false,
            isSecondOpen: false
          },

          //Calendar
          dt: null,
          today: today,
          options: {
            showWeeks: true
          }

        })

        //Calendar
        function today() {
          vm.dt = new Date();
        }

        today();

        //Modal Functions

        function toggleAnimation() {
          vm.animationsEnabled = !vm.animationsEnabled
        }
        //******** end *********//
      }
    }
  }

})();
