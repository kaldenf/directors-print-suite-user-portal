(function() {
  'use strict'
  angular.module('app').directive('support', supportController)

  function supportController() {
    return {

      templateUrl: 'pages/support.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          tab: 'contact'
        })

        init()

        function init() {
          videoPlayPause()
        }

        function videoPlayPause() {
          // when the modal is opened...
          $('#vid-modal1').on('show.bs.modal', function () {
            // call play() on the <video> DOM element
            $('#video1')[0].play()
          })
          $('#vid-modal1').on('hide.bs.modal', function () {
            // call play() on the <video> DOM element
            $('#video1')[0].pause()
          })

          // when the modal is opened...
          $('#vid-modal2').on('show.bs.modal', function () {
            // call play() on the <video> DOM element
            $('#video2')[0].play()
          })
          $('#vid-modal2').on('hide.bs.modal', function () {
            // call play() on the <video> DOM element
            $('#video2')[0].pause()
          })

          // when the modal is opened...
          $('#vid-modal3').on('show.bs.modal', function () {
            // call play() on the <video> DOM element
            $('#video3')[0].play()
          })
          $('#vid-modal3').on('hide.bs.modal', function () {
            // call play() on the <video> DOM element
            $('#video3')[0].pause()
          })
        }


        //******** end *********//
      }
    }
  }

})();
