angular.module('app').filter('replacespaces', function() {
    return function(input) {
        if (input) {
            return input.replace(/\s+/g, '%20');
        }
    }
});
