angular.module('app', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'slick', 'ui.checkbox','mp.colorPicker'])

angular.module('app').config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode({
		enabled: true
	})
	$routeProvider
	.when('/', {
		template: '<landing></landing>'
	})
		.when('/login', {
			template: '<login></login>'
		})
		.when('/signup', {
			template: '<signup></signup>'
		})
		.when('/signup/onboarding', {
			template: '<onboarding></onboarding>'
		})
		.when('/dashboard', {
			template: '<dashboard></dashboard>'
		})
		.when('/editor/:orderid/:productid/:layout', {
			template: '<editor></editor>'
		})
		.when('/order/:id', {
			template: '<order></order>'
		})
		.when('/order/:id/editor/:productId', {
			template: '<editor></editor>'
		})
		.when('/collections', {
			template: '<collections></collections>'
		})
		.when('/cases', {
			template: '<cases></cases>'
		})
		.when('/cases/case/:id', {
			template: '<case></case>'
		})
		.when('/settings', {
			template: '<settings></settings>'
		})
		.when('/staff/:id', {
			template: '<staff></staff>'
		})
		.when('/support', {
			template: '<support></support>'
		})
		.when('/resources', {
			template: '<resources></resources>'
		})
		.when('/style-guide', {
			template: '<style-guide></style-guide>'
		})
		.when('/inactive', {
			template: '<inactive></inactive>'
		})
		.when('/404', {
			templateUrl: 'pages/404.html'
		})
		.otherwise({
			redirectTo: '/login'
		})
})

angular.module('app').run(function($http, $rootScope, $location, auth) {
	if (localStorage.getItem('token') && localStorage.getItem('userId') && localStorage.getItem('funeralHomeId')) {
		$http.defaults.headers.common['x-access-token'] = localStorage.getItem('token')
		$http.defaults.headers.common['userid'] = localStorage.getItem('userId')
		$http.defaults.headers.common['funeralhomeid'] = localStorage.getItem('funeralHomeId')
	}
	$rootScope.$on('$routeChangeStart', function(next, current) {
    $rootScope.url = $location.path()
		if($location.path() !== '/login' && $location.path() !== '/signup' && $location.path() !== '/' && $location.path() !== '/inactive'){
			auth.verifyToken().then(function(response){
				if(response.data.success){
					if(response.data.active == false){
						window.location.href = '/inactive'
					}
				} else {
					window.location.href = '/login'
				}
			})
		}
	})
})

var api = 'https://bm-api-live.herokuapp.com/'
//var api = 'http://localhost:8080/'

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
