angular.module('app').service('assetLibraries', function($http, $q) {

  this.getAll = function() {
    return $q.when($http.get(api + 'asset-libraries'))
  }

  this.get = function(id) {
    return $q.when($http.get(api + 'asset-libraries/' + id))
  }

})
