angular.module('app').service('auth', function(user, $q, $rootScope) {

  this.verifyToken = function(){
    return $q.when(user.checkTokenStatus())
  }

})
