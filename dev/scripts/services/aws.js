angular.module('app').service('aws', function($http, $q) {

  this.getCSVSignedUrl = function(file, filename) {
    return $q.when($http.get(api + 'uploadcase/csv?file_name=' + filename + '&file_type=' + file.type + '', file))
  }

  this.getSignedUrl = function(file, filename) {
    return $q.when($http.get(api + 'funeral-homes/sign-url?file_name=' + filename + '&file_type=' + file.type + '', file))
  }

  this.uploadImage = function(file, signed_request) {
    var deferred = $q.defer()

    var xhr = new XMLHttpRequest();
    xhr.open("PUT", signed_request);
    xhr.setRequestHeader('x-amz-acl', 'public-read');
    xhr.onload = function() {
      if (xhr.status === 200) {
        deferred.resolve(true)
      }
    }
    xhr.onerror = function() {
      alert("Could not upload file.");
    };
    xhr.send(file);
    return deferred.promise
  }

})
