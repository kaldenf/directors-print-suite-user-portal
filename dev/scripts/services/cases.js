angular.module('app').service('cases', function($http, $q) {

  var service = this

  this.get = function(id) {
    return $q.when($http.get(api + 'cases/' + id))
  }

  this.getAll = function() {
    return $q.when($http.get(api + 'cases'))
  }

  this.create = function(newCase) {
    return $q.when($http.post(api + 'cases', newCase))
  }

  this.update = function(id, updatedCase) {
    return $q.when($http.post(api + 'cases/case/' + id, {data:updatedCase}))
  }

  this.delete = function(id) {
    return $q.when($http.delete(api + 'cases/case/' + id))
  }

  this.getCasesForOrders = function(orders) {
    var deferred = $q.defer()

    var currentRequest = 0

    function iterate() {
      if (currentRequest < orders.length) {
        if(orders[currentRequest].caseId){
          service.get(orders[currentRequest].caseId).then(function(data) {
            orders[currentRequest].case = data
            currentRequest++
            iterate()
          })
        } else {
          orders[currentRequest].case = null
          currentRequest++
          iterate()
        }
      } else {
        deferred.resolve(orders)
      }
    }
    iterate()

    return deferred.promise
  }

  this.getCSVData = function(url){
    return $q.when($http.get(url))
  }

})
