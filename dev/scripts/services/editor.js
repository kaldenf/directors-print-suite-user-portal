var collectionProductSchema = {
  //direct values from global product
  name: String,
  thumbnail: String,
  styles: Object,

  //generated values from global product depending on collections paperType
  paperType: String,
  startPoint: Object,
  columns: Number,
  rows: Number,
  blockSize: Object,
  paperSize: Object,
  finishedProductSize: Object,
  selectedLayout: Number, // index of selected layout in layouts
  id: String, // generated random string (5)
  layouts: Array // e.g. 'white border' ( template )
} //what a product will look like in a collection


var globalProductSchema = {
  name: String,
  thumbnail: String,
  styles: Object,
  paperTypes: Array //( name, startPoint, columns, rows, blockSize, paperSize, finishedProductSize, layouts ) generates { id, selectedLayout }
} 
