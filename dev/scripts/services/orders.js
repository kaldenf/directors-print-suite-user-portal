angular.module('app').service('orders', function($http, $q) {

  this.get = function(id) {
    return $q.when($http.get(api + 'orders/' + id))
  }

  this.getAll = function() {
    return $q.when($http.get(api + 'orders'))
  }

  this.getRecent = function(count) {
    return $q.when($http.get(api + 'orders/recent/' + count))
  }

  this.create = function(order) {
    return $q.when($http.post(api + 'orders/', {
      data: order
    }))
  }

  this.update = function(order) {
    var object = {
      caseId: order.caseId,
      collections: order.collections,
      theme: order.theme,
      archived: order.archived,
      complete: order.complete
    }
    console.log(object)
    return $q.when($http.post(api + 'orders/order/' + order._id, {
      data: object
    }))
  }

})
