angular.module('app').service('products', function($http, $q) {

  var service = this

  this.getAll = function(){
    return $q.when($http.get(api + 'products'))
  }

  this.getCollectionProducts = function(collections){
    var deferred = $q.defer()

    var i = 0

    function iterate() {
      if (i < collections.data.length) {
        service.getAll().then(function(products) {
          for(var a in products.data){
            for(var b in collections.data[i].products){
              if(products.data[a]._id == collections.data[i].products[b].id){
                collections.data[i].products[b].details = products.data[a]
                collections.data[i].products[b].collection = collections.data[i].name
              }
            }
          }

          i++
          iterate()
        })
      } else {
        deferred.resolve(collections)
      }
    }
    iterate()

    return deferred.promise
  }

})
