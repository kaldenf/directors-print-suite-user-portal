angular.module('app').service('themeCategories', function($http, $q) {

  this.get = function(id) {
    return $q.when($http.get(api + 'theme-categories/' + id))
  }

  this.getAll = function() {
    return $q.when($http.get(api + 'theme-categories'))
  }

})
