angular.module('app').service('themes', function($http, $q) {

  var service = this

  this.get = function(id) {
    return $q.when($http.get(api + 'themes/' + id))
  }

  this.getAll = function(id) {
    return $q.when($http.get(api + 'themes/'))
  }

  this.getThemesForCategories = function(categories){
    var deferred = $q.defer()

    var i = 0

    function iterate() {
      if (i < categories.data.length) {
        categories.data[i].themes = []
        service.getAll().then(function(themes) {
          for(var a in themes.data){
            if(themes.data[a].category == categories.data[i]._id){
              categories.data[i].themes.push(themes.data[a])
            }
          }
          i++
          iterate()
        })
      } else {
        deferred.resolve(categories)
      }
    }
    iterate()

    return deferred.promise
  }



})
