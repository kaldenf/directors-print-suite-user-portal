angular.module('app').service('user', function($http, $q) {

  this.get = function() {
    return $q.when($http.get(api + 'users/user'))
  }

  this.update = function(user) {
    return $q.when($http.post(api + 'users/user', {data:user}))
  }

  this.getIpLocation = function() {
    return $q.when($http.get(api + 'users/iplocation'))
  }

  this.login = function(user) {
    var deferred = $q.defer()
    $http.post(api + 'users/verify', {
        data: user
      })
      .success(function(data) {
        localStorage.setItem('token', data.token)
        localStorage.setItem('userId', data.userId)
        localStorage.setItem('funeralHomeId', data.funeralHomeId)
        deferred.resolve(data)
      })
      .error(function(data) {
        //error
      })
    return deferred.promise
  }

  this.logout = function() {
    localStorage.removeItem('token')
    localStorage.removeItem('userId')
    localStorage.removeItem('funeralHomeId')
    window.location.href = '/login'
  }

	this.checkEmail = function(email) {
		return $q.when($http.post(api + 'users/checkemail', {data:{
			email: email
		}}))
	}

  this.signup = function(form) {
    var deferred = $q.defer()
      //create funeral home to get id
    $http.post(api + 'funeral-homes', {
        data: {
          name: form.details.funeralHome,
          logo: 'http://placehold.it/100x100'
        }
      })
      .success(function(response) {
        //create user with funeral home id
        $http.post(api + 'users', {
            data: {
              funeralHomeId: response._id,
              name: form.name,
              email: form.email,
              password: form.password,
              details: form.details
            }
          })
          .success(function(data) {
            localStorage.setItem('token', data.token)
            localStorage.setItem('userId', data.userId)
            localStorage.setItem('funeralHomeId', response._id)
            deferred.resolve(data)
						window.location.href = '/dashboard'
          })
          .error(function(data) {
            //error
          })
      })

    return deferred.promise
  }

  this.addStaff = function(form) {
    var object = {
      email: form.email,
      name: form.name,
      funeralHomeId: form.funeralHomeId,
      funeralHomeName: form.funeralHomeName
    }
    return $q.when($http.post(api + 'users/staff', {
      data: object
    }))
  }

  this.removeStaff = function(id) {
    return $q.when($http.delete(api + 'users/' + id))
  }

  this.getStaffMembers = function() {
    return $q.when($http.get(api + 'users/staff'))
  }

  this.activate = function(id, form) {
    var deferred = $q.defer()
    var object = {
      email: form.email,
      password: form.password,
      activationId: id
    }
    $http.post(api + 'users/staff/activate', {
        data: object
      })
      .success(function(data) {
        localStorage.setItem('token', data.token)
        localStorage.setItem('userId', data.userId)
        localStorage.setItem('funeralHomeId', data.funeralHomeId)
        deferred.resolve(true)
        window.location.href = '/dashboard'
      })
      .error(function(data) {
        //error
      })

    return deferred.promise
  }

  this.checkTokenStatus = function() {
    return $q.when($http.post(api + 'users/verifyToken'))
  }

  this.resendActivationEmail = function(activationId, name, email) {
    var object = {
      funeralHomeName: name,
      activationId: activationId,
      email: email
    }
    return $q.when($http.post(api + 'users/staff/resend-activation', {
      data: object
    }))
  }

})
