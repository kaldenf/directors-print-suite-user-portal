var connect = require('connect');
var superstatic = require('superstatic');
var open = require('open');

var app = connect();

app.use(superstatic({
  config: {
    root: "./dist",
    clean_urls: true,
    routes: {
      "**": "index.html"
    }
  }
}));

app.listen(process.env.PORT || 8004);
console.log('App running on http://localhost:8004')
open('http://localhost:8004')
